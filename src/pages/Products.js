// import coursesData from '../data/coursesData';
import { useEffect, useState } from 'react';
import ProductCard from '../components/ProductCard';

export default function Products() {

	// State that will be used to store the courses retrieved from the database
	const [ products, setProducts ] = useState([]);

	// Retrieves the courses from the database upon intial render of the "Courses" component

	useEffect(() => {
		fetch(`${ process.env.REACT_APP_API_URL }/products`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			// Sets the "courses" state to map the data retrieved from the fetch request into several "CourseCard" components
			setProducts(data.map(product => {
				return (
					<ProductCard key={product._id} productProp = {product}/>
				)
			}))
		})
	}, [])

	return (
		<>
			<h1>Products</h1>
			{products}
			
		</>

	)
}