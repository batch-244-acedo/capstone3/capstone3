// import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Card, Button } from 'react-bootstrap';

export default function ProductCard({productProp}) {

	// Decontruct the product properties into their own variables
	const { _id, name, description, price} = productProp;

    return (
	    <Card>
	        <Card.Body>
	            <Card.Title>{name}</Card.Title>
	            <Card.Subtitle>Description:</Card.Subtitle>
	            <Card.Text>{description}</Card.Text>
	            <Card.Subtitle>Price:</Card.Subtitle>
	            <Card.Text>Php {price}</Card.Text>
	            <Button as={Link} variant="primary" to={`/products/${_id}`}>More</Button>
	        </Card.Body>
	    </Card>  

    )
}